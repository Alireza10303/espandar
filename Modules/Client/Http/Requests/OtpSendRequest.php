<?php

namespace Modules\Client\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class OtpSendRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

            if(filter_var(Request::input('identify'),FILTER_VALIDATE_EMAIL)){
               return [
                   'identify'=>'required|regex:/^[a-zA-Z]/|exists:clients,email',
               ];
            }else
            {
                return [
                    'identify'=>'required|regex:/(09)[0-9]{9}/|exists:clients,mobile',
                ];
            }

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
