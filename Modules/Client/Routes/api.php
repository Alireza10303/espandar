<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'auth'],function (){
    Route::post('/sign','RestController@sign');
    Route::post('/verify','RestController@verify');
    Route::post('/otp','RestController@otp');
    Route::get('/revoke','RestController@revoke')->middleware('api');
});


Route::group(['middleware'=>'api'],function (){

    Route::group(['prefix'=>'account'],function (){
        Route::get('/','RestController@show');
    });


});
