<?php


namespace Modules\Client\Helper;


class ClientHelper
{

    public static function active($active){
        switch ($active){
            case 1 :
                return '<span class="alert-success">تایید شده</span>';
                break;
            case 2 :
                return '<span class="alert-danger">تایید نشده</span>';
                break;
            default:
                return '<span class="status-danger">خطای سیستمی</span>';
                break;

        }
    }
    public static function activeValue($active)
    {
        switch ($active) {
            case 1 :
                return 'تایید نشده';
                break;
            case 2 :
                return 'تایید شده';
                break;

            default:
                return 'خطای سیستمی';
                break;

        }
    }
    public static function plus($plus){
        switch ($plus){
            case 1 :
                return '<span class="alert-success">کاربر عادی</span>';
                break;
            case 2 :
                return '<span class="alert-primary">کاربر ویژه</span>';
                break;
            default:
                return '<span class="status-danger">خطای سیستمی</span>';
                break;

        }
    }
    public static function plusValue($plus)
    {
        switch ($plus) {
            case 0 :
                return 'کاربر عادی';
                break;
            case 1 :
                return 'کاربر ویژه';
                break;

            default:
                return 'خطای سیستمی';
                break;

        }
    }

}
