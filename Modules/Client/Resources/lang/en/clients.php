<?php
return [
    "store"=>"Store Success",
    "error"=>"The system has encountered a problem",
    "error-password"=>"error password",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"members list",
    "singular"=>"client",
    "collect"=>"clients",
    "permission"=>[
        "client-full-access"=>"client full access",
        "client-list"=>"client list",
        "client-delete"=>"client delete",
        "client-create"=>"client create",
        "client-edit"=>"edit client",
    ]
];
