<li>
        <a href="{{route('clients.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('client.icons.client')}}"></i>
                              </span>
            <span class="nav-text">{{__('client::clients.collect')}}</span>
        </a>
</li>
