<?php
return [
    "text-create"=>"you can create your lottery",
    "text-edit"=>"you can edit your lottery",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"lotteries list",
    "error"=>"error",
    "singular"=>"lottery",
    "collect"=>"lotteries",
    "permission"=>[
        "lottery-full-access"=>"lotteries full access",
        "lottery-list"=>"lotteries list",
        "lottery-delete"=>"lottery delete",
        "lottery-create"=>"lottery create",
        "lottery-edit"=>"edit lottery",
    ]


];
