<?php


namespace Modules\Sms\Http\Controllers\Facade;


use Illuminate\Support\Facades\Facade;

class KavenegarFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'kavenegar';
    }
}
