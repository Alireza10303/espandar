<?php

namespace Modules\Sms\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Mockery\Exception;
use Modules\Sms\Entities\Sms;
use Modules\Sms\library\SmsHelper;

class SmsController extends Controller
{

    protected $entity;

    public function __construct()
    {
        $this->entity=new Sms();

        $this->middleware('permission:sms-list')->only(['only'=>['index']]);
        $this->middleware('permission:sms-create')->only(['create','store']);
        $this->middleware('permission:sms-edit' )->only(['edit','update']);
        $this->middleware('permission:sms-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $smses=Sms::latest()->get();
            return view('sms::layouts.index',compact('smses'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('sms::layouts.create');
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $provider=null;
            foreach (SmsHelper::SmsProvider() as $key=>$value){
                $provider=($key==$request->input('provider'))  ? $value : null;
            }

            if(!is_null($provider)){
                Sms::create([
                    "provider"=>$provider,
                    "key"=>$request->input('token'),
                    "status"=>checkboxStatus($request->input('status')),
                    "token"=>tokenGenerate()
                ]);
                return redirect(route("smses.index"));
            }
            return redirect()->back()->with('error','شرکت مورد نظر یافت نشد');



        }catch (Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }



    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        try {
            return view('sms::edit');
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
}
