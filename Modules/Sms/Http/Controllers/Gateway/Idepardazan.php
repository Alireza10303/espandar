<?php


namespace Modules\Sms\Http\Controllers\Gateway;


use Illuminate\Support\Facades\Facade;
use Modules\Sms\Contract\SmsGatewayInterface;

class Idepardazan extends Facade implements SmsGatewayInterface
{

    protected static function getFacadeAccessor()
    {
       return 'Idepardazan';
    }

    public static function sendMessage($data)
    {
        $token=self::getToken();

        $body= array (
            'ParameterArray' =>
                array (
                        array (
                            'Parameter' => 'code',
                            'ParameterValue' => $data['code'],
                        ),
                        array (
                            'Parameter' => 'website',
                            'ParameterValue' => 'asonkala.ir',
                        ),
                ),
            'Mobile' => $data['mobile'],
            'TemplateId' => '40081',
        );
        $client = new \GuzzleHttp\Client();
        $res=$client->post('https://RestfulSms.com/api/UltraFastSend', [
            'headers'         => ['x-sms-ir-secure-token' =>$token, 'Content-Type'=> 'application/json'],
            'body'            =>  json_encode($body),
        ]);
        return $res->getStatusCode();

    }

    public static function getToken()
    {
        $body=[
            "UserApiKey"=>"95b77f127c273efd86a38b53",
	         "SecretKey"=>"developerphp"
        ];
        $client = new \GuzzleHttp\Client();
        $res=$client->post('https://RestfulSms.com/api/Token', [
            'headers'         => ['Content-Type'=> 'application/json'],
            'body'            =>  json_encode($body),
        ]);
       $result=json_decode($res->getBody()->getContents());

       return $result->TokenKey;
    }
}
