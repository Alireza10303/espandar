<?php

return [
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست پیام ها",
    "singular"=>"پیام",
    "collect"=>"پیام ها",
    "permission"=>[
        "sms-full-access"=>"دسترسی کامل به پیام ها ",
        "sms-list"=>"لیست پیام ها ",
        "sms-delete"=>"حذف پیام",
        "sms-create"=>"ایجاد پیام",
        "sms-edit"=>"ویرایش پیام",
    ]
];
