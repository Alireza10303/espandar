<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\City;
use Modules\Core\Entities\Country;

class AreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('areas')->delete();

        $areas=[
            [
                'name' => 'Jordan',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'Beryanak',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'Vanak',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'TehranPars',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'Ekbatan',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'YousefAbad',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'qeytarieh',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'niavaran',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'SaadatAbad',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'ShahrakeGharb',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'KarimKhan',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'Gisha',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'Velenjak',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'EnghelabSquare',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'Elahiyeh',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'MehrabadInternationalAirport',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'ImamKhomeiniInternationalAirport',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'BagheFeyz',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'Afsariyeh',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'TehranBazaar',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'AmirAbad',
                'city' => City::where('name','Tehran')->first()->id,
                'created_at' =>now(),
            ],

                [
                    'name' => 'Baharestan',
                    'city' => City::where('name','Tehran')->first()->id,
                    'created_at' =>now(),
                ],
                [
                    'name' => 'Piroozi',
                    'city' => City::where('name','Tehran')->first()->id,
                    'created_at' =>now(),
                ]


        ];

        DB::table('areas')->insert($areas);
    }
}
