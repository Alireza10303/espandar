@if(is_null($item))


    <div class="box-body p-v-md">
        <div class="row row-sm">
            <div class="form-group row">
                <div class="col-sm-12">
                    <span class="text-danger">*</span>
                    <label for="kinds" class="form-control-label">{{__('cms.kind_advertising')}} </label>
                    <select dir="rtl" class="form-control"  id="kinds" name="kinds[]" required multiple>
                        @foreach($types as $key=>$value)

                            <option value="{{$value->title}}">{{$value->symbol}}</option>

                        @endforeach
                    </select>
                </div>
            </div>

        </div>
    </div>
@else

    <div class="box-body p-v-md">
        <div class="row row-sm">

            <div class="form-group row">
                <div class="col-sm-12">
                    <span class="text-danger">*</span>
                    <label for="kinds" class="form-control-label">{{__('cms.kind_advertising')}} </label>

                    <select dir="rtl" class="form-control"  id="kinds" name="kinds[]" required multiple>
                        @foreach($types as $key=>$value)


                            @if($item->kind!='null')
                                <option value="{{$value->title}}" {{in_array($value->title,json_decode($item->kind,true)) ? "selected": ""}}>{{$value->symbol}}</option>
                            @else
                                <option value="{{$value->title}}" >{{$value->symbol}}</option>
                            @endif
                        @endforeach
                    </select>

                </div>
            </div>

        </div>
    </div>
@endif





