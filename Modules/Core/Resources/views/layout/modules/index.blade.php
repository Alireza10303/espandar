@extends('core::layout.panel')
@section('pageTitle', $title)
@section('content')

    <div class="padding">

        @include('core::layout.alert-success')


        @if(isset($search_route) && $search_route)
        @include($parent.'::'.$directory.'.search')
        @endif



        <div class="row">
            <div class="col-sm-12">

                <div class="box">

                    <div class="box-header">
                        <div class="pull-left">
                            <h2>{{$collect}}</h2>
                            <small>{{$collect}}</small>
                        </div>

                            @if(isset($parent_route))
                                @if(isset($custom))
                                <a title="{{__('cms.categories')}}" href="{{route($custom.'s.index')}}" class="btn btn-sm text-sm text-sm-center btn-warning pull-right"><i class="fa fa-reply"></i>  </a>
                            @else
                                <a title="{{__('cms.categories')}}" href="{{route($model.'s.index')}}" class="btn btn-sm text-sm text-sm-center btn-warning pull-right"><i class="fa fa-reply"></i>  </a>

                            @endif
                            @endif

                        @if(isset($setting_route))
                            <a title="{{__('cms.setting')}}" href="#" class="btn btn-sm text-sm text-sm-center btn-warning pull-right"><i class="fa fa-gear"></i>  </a>
                        @endif
                        @if(isset($add_student_route_classroom))
                            <a title="{{__('cms.add_student')}}" href="{{route("student.show.classroom.force.add",['classroom'=>$data->id])}}" class="btn btn-sm text-sm text-sm-center btn-info pull-right"><i class="fa fa-user-plus"></i>  </a>
                        @endif
                        @if(isset($add_student_route_race))
                            <a title="{{__('cms.add_student')}}" href="{{route("student.show.race.force.add",['race'=>$data->id])}}" class="btn btn-sm text-sm text-sm-center btn-info pull-right"><i class="fa fa-user-plus"></i>  </a>
                        @endif


                        @can($model.'-create')

                            @if(isset($create_route) && isset($create_route['param']))

                                <a title="{{__('cms.add')}}" href="{{route($create_route['name'],[$model=>$create_route['param']])}}" class="btn btn-sm text-sm text-sm-center btn-primary pull-right"><span class="{{config('cms.icon.add')}}"></span>  </a>

                            @elseif(isset($create_route))
                                <a title="{{__('cms.add')}}" href="{{route($create_route['name'])}}" class="btn btn-sm text-sm text-sm-center btn-primary pull-right"><span class="{{config('cms.icon.add')}}"></span>  </a>

                            @endif

                                @if(isset($role_route))

                                    <a title="{{__('cms.role')}}" href="{{route('roles.index')}}" class="btn btn-sm text-sm text-sm-center btn-warning pull-right"><span class="{{config('cms.icon.role')}}"></span>  </a>

                                @endif

                            @endcan



                        @can($model.'-create')
                            @if(isset($category_route))
                                <a title="{{__('cms.categories')}}" href="{{route($model.'.categories')}}" class="btn btn-sm text-sm text-sm-center btn-warning pull-right"><span class="{{config('cms.icon.categories')}}"></span>  </a>
                            @endif
                                @if(isset($leader_route))
                                    <a title="{{__('cms.leaders')}}" href="{{route($model.'.leaders')}}" class="btn btn-sm text-sm text-sm-center btn-success pull-right"><span class="{{config('cms.icon.leaders')}}"></span>  </a>
                                @endif
                        @endcan


                    </div>
                    <div class="table-responsive">

                        @include('core::layout.modules.datatable',['datatable'=>$datatable])
                        @if(isset($pagination) && $pagination)
                            {!! $items->links() !!}
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
