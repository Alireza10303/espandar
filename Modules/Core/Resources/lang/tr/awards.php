<?php
return [
    "text-create"=>"you can create your award",
    "text-edit"=>"you can edit your award",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"awards list",
    "error"=>"error",
    "singular"=>"award",
    "collect"=>"awards",
    "permission"=>[
        "award-full-access"=>"award full access",
        "award-list"=>"awards list",
        "award-delete"=>"award delete",
        "award-create"=>"award create",
        "award-edit"=>"edit award",
    ]


];
