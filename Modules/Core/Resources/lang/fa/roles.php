<?php

return [
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست نقش ها",
    "singular"=>"نقش",
    "collect"=>"نقش ها",
    "permission"=>[
        "role-full-access"=>"دسترسی کامل به نقش",
        "role-delete"=>"حذف نقش",
        "role-create"=>"ایجاد نقش",
        "role-edit"=>"ویرایش نقش",
    ]
];
