@include('core::layout.modules.category.edit',[

    'title'=>__('core::categories.create'),
    'item'=>$item,
    'parent'=>'advertising',
    'model'=>'advertising',
    'directory'=>'advertisings',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'update_route'=>['name'=>'advertising.category.update','param'=>'category'],

])








