<?php


namespace Modules\Order\Entities\Repository;


use Modules\Order\Entities\Order;

class OrderRepository implements OrderRepositoryInterface
{

    public function getAll()
    {
        return Order::with('payment')->get();
    }
}
