<?php

namespace Modules\Order\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Order\Entities\Order;

class OrderCollection extends ResourceCollection
{
    public $collects = Order::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new OrderResource($item);
                }
            ),
            'filed' => [
                'order_id','title','full_name','mobile','email','price','status','update_date','create_date'
            ],
            'public_route'=>[

            ],
            'private_route'=>
                [

                ],
            'search_route'=>[

                'name'=>'search.order',
                'filter'=>[
                    'title','order_id'
                ],
                'icon'=>config('cms.icon.add'),
                'title'=>__('cms.add'),
                'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                'method'=>'POST',

            ],
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('order::orders.collect'),
            'title'=>__('order::orders.index'),
        ];
    }
}
