<?php
return [
    "text-create"=>"you can create your menu",
    "text-edit"=>"you can edit your menu",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"menus list",
    "error"=>"error",
    "singular"=>"menu",
    "collect"=>"menus",
    "permission"=>[
        "menu-full-access"=>"menu full access",
        "menu-list"=>"menus list",
        "menu-delete"=>"menu delete",
        "menu-create"=>"menu create",
        "menu-edit"=>"edit menu",
    ]
];
