<?php

namespace Modules\Menu\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Information\Entities\Information;
use Modules\Menu\Entities\Menu;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Menu::class)->delete();

        Permission::create(['name'=>'menu-list','model'=>Menu::class,'created_at'=>now()]);
        Permission::create(['name'=>'menu-create','model'=>Menu::class,'created_at'=>now()]);
        Permission::create(['name'=>'menu-edit','model'=>Menu::class,'created_at'=>now()]);
        Permission::create(['name'=>'menu-delete','model'=>Menu::class,'created_at'=>now()]);
    }
}
