<?php


namespace Modules\Menu\Helper;



use Illuminate\Database\Eloquent\Model;
use Modules\Menu\Entities\Menu;

class MenuHelper
{
    public static function checkSubMenu(Model $menu,$token){

        if($menu->parent==0){
            return "";
        }
        else{
            if(Menu::whereId($menu->parent)->first()->token==$token){
                return "selected";
            }
           return "";
        }

    }


}
