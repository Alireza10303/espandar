<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(User::class)->delete();

        Permission::create(['name'=>'user-list','model'=>User::class,'created_at'=>now()]);
        Permission::create(['name'=>'user-create','model'=>User::class,'created_at'=>now()]);
        Permission::create(['name'=>'user-edit','model'=>User::class,'created_at'=>now()]);
        Permission::create(['name'=>'user-delete','model'=>User::class,'created_at'=>now()]);



        DB::table('permissions')->whereModel(Role::class)->delete();


        Permission::create(['name'=>'role-list','model'=>Role::class,'created_at'=>now()]);
        Permission::create(['name'=>'role-create','model'=>Role::class,'created_at'=>now()]);
        Permission::create(['name'=>'role-edit','model'=>Role::class,'created_at'=>now()]);
        Permission::create(['name'=>'role-delete','model'=>Role::class,'created_at'=>now()]);

    }
}
