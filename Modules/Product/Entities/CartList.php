<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class CartList extends Model
{
    protected $fillable = ['cart','product','count','price'];

    public function product_item(){

        return $this->belongsTo(Product::class,'product','id')->with('price');

    }

}
