<?php


namespace Modules\Product\Entities\Repository\OptionProperty;


use Modules\Core\Entities\Option;
use Modules\Product\Entities\OptionProperty;

class OptionPropertyRepository implements OptionPropertyRepositoryInterface
{

    public function getAll()
    {
        return OptionProperty::latest()->get();
    }

    public function getOption($token)
    {
        $product=OptionProperty::whereToken($token)->firstOrFail();
        return Option::latest()->where('optionable_type',OptionProperty::class)->where('optionable_id',$product->id)->get();
    }
}
