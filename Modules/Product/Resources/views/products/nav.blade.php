<li>
        <a href="{{route('products.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="material-icons">{{config('product.icons.product')}} </i>
                              </span>
            <span class="nav-text">{{__('product::products.collect')}}</span>
        </a>
    </li>
