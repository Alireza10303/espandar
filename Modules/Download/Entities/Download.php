<?php

namespace Modules\Download\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Question\Entities\Question;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Download extends Model implements HasMedia
{
    use HasMediaTrait,Sluggable,TimeAttribute;

    protected $fillable = ['title','text','excerpt','token','slug','user','order','category'];

    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }
    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }


    public  function getViewAttribute(){

        return $this->analyzer->view;
    }

    public  function getLikeAttribute(){

        return $this->analyzer->like;
    }

    public  function getQuestionAttribute(){

        return $this->questions()->count();
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
