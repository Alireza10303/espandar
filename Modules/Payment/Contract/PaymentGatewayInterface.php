<?php


namespace Modules\Payment\Contract;


use Illuminate\Database\Eloquent\Model;

interface PaymentGatewayInterface
{
    public static function request($data,Model $model);
    public static function response();
    public static function discount($amount);

}
