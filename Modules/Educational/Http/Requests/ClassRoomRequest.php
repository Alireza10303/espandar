<?php

namespace Modules\Educational\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClassRoomRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'sign_time_start'=>'required|regex:/^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]+$/',
            'time_start'=>'required|regex:/^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]+$/',
            'date_start'=>'required',
            'time_end'=>'required',
            'date_end'=>'required',
            'professor'=>'required',
            'text'=>'required',
            'excerpt'=>'required',
            'event_place'=>'required',
            'currency'=>'required',
            'total_hour'=>'required|numeric|min:1',
            'leader'=>'required',
            'order'=>'required|numeric|min:1',
            'price'=>'numeric|min:0'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
