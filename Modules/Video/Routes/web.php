<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::resource('/videos', 'VideoController')->only('create','store','destroy','update','index','edit');

    Route::group(["prefix"=>'search'], function () {
        Route::post('/videos', 'VideoController@search')->name('search.videos');
    });

    Route::group(["prefix"=>'video/categories'], function () {
        Route::get('/', 'VideoController@categories')->name('video.categories');
        Route::get('/create', 'VideoController@categoryCreate')->name('video.category.create');
        Route::post('/store', 'VideoController@categoryStore')->name('video.category.store');
        Route::get('/edit/{category}', 'VideoController@categoryEdit')->name('video.category.edit');
        Route::patch('/update/{category}', 'VideoController@categoryUpdate')->name('video.category.update');

    });

    Route::group(["prefix"=>'gallery/questions'], function () {
        Route::get('/{video}', 'VideoController@question')->name('video.questions');
        Route::get('/create/{video}', 'VideoController@questionCreate')->name('video.question.create');
        Route::post('/store/{video}', 'VideoController@questionStore')->name('video.question.store');
        Route::delete('/destroy/{question}', 'VideoController@questionDestroy')->name('video.question.destroy');
        Route::get('/edit/{video}/{question}', 'VideoController@questionEdit')->name('video.question.edit');
        Route::patch('/update/{question}', 'VideoController@questionUpdate')->name('video.question.update');
    });

});
