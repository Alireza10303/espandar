<?php

namespace Modules\Video\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VideoRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|unique:videos,title,'.$this->token.',token',
            'text'=>'required',
            'excerpt'=>'required',
            'category'=>'required',
            'status'=>'required',
            'image'=>'mimes:jpeg,png,jpg|max:2000',
            'video'=>'mimes:mp4|file|max:9000'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
