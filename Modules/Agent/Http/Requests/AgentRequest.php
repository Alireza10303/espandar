<?php

namespace Modules\Agent\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'=>'required',
            'lastname'=>'required',
            'password'=>'required|min:8',
            'mobile'=>'required|regex:/(09)[0-9]{9}/|unique:agents,mobile,'.$this->token.',token',
            'email'=>'required|unique:agents,email,'.$this->token.',token',
            'order'=>'required|numeric|min:1',
            'branch'=>'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
