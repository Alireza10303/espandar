<?php


namespace Modules\Plan\Helper;


use Modules\Advertising\Entities\Period;

class PlanHelper
{
    public static function status($period){
        switch ($period){
            case 1 :
                return '<span class="alert-success"> فعال</span>';
                break;
            case 2 :
                return '<span class="alert-danger">غیرفعال </span>';
                break;
            default:
                return '<span class="alert-warning">خطای سیستمی</span>';
                break;

        }
    }
    public static function special($special){
        switch ($special){
            case 1 :
                return '<span class="alert-info">عادی</span>';
                break;
            case 2 :
                return '<span class="alert-warning">ویژه</span>';
                break;
            default:
                return '<span class="alert-danger">خطای سیستمی</span>';
                break;

        }
    }

    public static function period($period){
        switch ($period){
            case 0 :
                return '<span class="alert-success">ماهانه</span>';
                break;
            case 1 :
                return '<span class="alert-success">سه ماهه</span>';
                break;
            case 2 :
                return '<span class="alert-success">شش ماهانه</span>';
                break;
            case 3 :
                return '<span class="alert-success">سالانه</span>';
                break;
            default:
                 return '<span class="alert-danger">خطای سیستمی</span>';
                 break;

        }
    }
    public static function periodValue($period){

        return Period::find($period)->symbol;
    }
    public static function periodCalc($period){

        $duration=Period::find($period)->duration;

        return now()->addDays($duration);

    }

}
