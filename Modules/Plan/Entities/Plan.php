<?php

namespace Modules\Plan\Entities;

use App\Events\Order;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Entities\Discount;
use Modules\Core\Entities\Notification;
use Modules\Core\Entities\Price;
use Modules\Core\Entities\UserAction;
use Modules\Core\Entities\UserCurrency;
use Modules\Core\Helper\Trades\CommonAttribute;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Order\Entities\OrderList;
use Modules\Plan\Helper\PlanHelper;
use Modules\Question\Entities\Question;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;


class Plan extends Model implements HasMedia
{
    use CommonAttribute,Sluggable,HasMediaTrait,TimeAttribute;

    protected $fillable = ['title','special','text','excerpt','price','period','token','attributes','order','slug','time_limit','number_limit_special','number_limit','status'];


    public function getRouteKeyName()
    {
        return multiRouteKey();
    }
    public function orderList()
    {
        return $this->morphOne(OrderList::class, 'orderable');
    }
    public function userAction()
    {
        return $this->morphMany(UserAction::class, 'actionable');
    }
    public function price()
    {
        return $this->morphOne(Price::class, 'priceable');
    }
    public function discount()
    {
        return $this->morphOne(Discount::class, 'discountable');
    }
    public function order()
    {
        return $this->morphOne(Order::class, 'orderable');
    }
    public function currency()
    {
        return $this->morphOne(UserCurrency::class, 'userable');
    }
    public function notification()
    {
        return $this->morphOne(Notification::class, 'userable');
    }
    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }

    public  function getShowSpecialAttribute(){

        return PlanHelper::special( ($this->special));
    }

    public  function getPriceFormatAttribute(){

        return number_format($this->price->amount);
    }

     /**
      * @inheritDoc
      */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
