<?php

namespace Modules\Gallery\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GalleryRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|unique:galleries,title,'.$this->token.',token',
            'text'=>'required',
            'category'=>'required',
            'excerpt'=>'required',
            'status'=>'required',
            'image'=>'mimes:jpeg,png,jpg|max:2000'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
