<?php

namespace Modules\Store\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Store extends Model implements HasMedia
{
    use HasMediaTrait,Sluggable,TimeAttribute;

    protected $fillable = ['user','title','slug','category','text','order','excerpt','google_map','longitude','latitude','status','token'];


    public function getRouteKeyName()
    {
        return multiRouteKey();
    }
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(200)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));
    }
    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
