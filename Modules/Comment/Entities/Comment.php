<?php

namespace Modules\Comment\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;

class Comment extends Model
{
     use TimeAttribute;

    protected $table="cms_comments";

    protected $fillable = ['text','status','commentable_id','commentable_type','status'];

    public function commentable()
    {
        return $this->morphTo();
    }
}
