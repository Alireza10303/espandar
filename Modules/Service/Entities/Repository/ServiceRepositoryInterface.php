<?php


namespace Modules\Service\Entities\Repository;


interface ServiceRepositoryInterface
{
    public function getAll();

}
