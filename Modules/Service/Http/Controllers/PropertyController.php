<?php

namespace Modules\Service\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Service\Entities\Property;
use Modules\Service\Http\Requests\PropertyRequest;

class PropertyController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new Property();

        $this->middleware('permission:property-list')->only(['only'=>['index']]);
        $this->middleware('permission:property-create')->only(['create','store']);
        $this->middleware('permission:property-edit' )->only(['edit','update']);
        $this->middleware('permission:property-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(3);
            return view('service::properties.index',compact('items'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('service::properties.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",trim($request->title))
                ->paginate(config('cms.paginate'));
            return view('service::properties.index',compact('items','request'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('service::properties.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(PropertyRequest $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->icon=$request->input('icon');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->text=$request->input('text');
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->token=tokenGenerate();


            $saved=$this->entity->save();

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                return redirect()->back()->with('error',__('service::properties.error'));
            }else{
                return redirect(route("properties.index"))->with('message',__('service::properties.store'));
            }



        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->with('translates')->whereToken($token)->firstOrFail();
            return view('service::properties.edit',compact('item'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return Response
     */
    public function update(PropertyRequest $request, $token)
    {
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                "user"=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "icon"=>$request->input('icon'),
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
                "order"=>orderInfo($request->input('order'))
            ]);

            $this->entity->replicate();

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$updated){
                return redirect()->back()->with('error',__('service::properties.error'));
            }else{
                return redirect(route("properties.index"))->with('message',__('service::properties.update'));
            }


        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {

        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('service::properties.error'));
            }else{
                return redirect(route("properties.index"))->with('message',__('service::properties.delete'));
            }
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public  function languageShow(Request $request,$lang,$token){
        $item=Property::with('translates')->where('token',$token)->first();
        return view('service::properties.language',compact('item','lang'));

    }
    public  function languageUpdate(Request $request,$lang,$token){

        DB::beginTransaction();
        $item=Property::with('translates')->where('token',$token)->first();
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'excerpt'=>$request->excerpt,
                'title'=>$request->title,
                'text'=>$request->text,
            ]);
        }else{
            $changed=$item->translates()->create([
                'excerpt'=>$request->excerpt,
                'title'=>$request->title,
                'text'=>$request->text,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('property::properties.error'));
        }else{
            DB::commit();
            return redirect(route("properties.index"))->with('message',__('property::properties.update'));
        }

    }
}
