<?php


namespace Modules\Service\Helper;


use Illuminate\Database\Eloquent\Model;
use Modules\Service\Entities\Service;

class ServiceHelper
{
    public static function checkSubMenu(Model $menu,$token){

        if($menu->parent==0){
            return "";
        }
        else{
            if(Service::whereToken($token)->first()->token==$menu->token){
                return "selected";
            }
            return "";
        }

    }

}
