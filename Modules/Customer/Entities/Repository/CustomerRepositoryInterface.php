<?php


namespace Modules\Customer\Entities\Repository;


interface CustomerRepositoryInterface
{
    public function getAll();

}
