<?php

namespace Modules\Ad\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'href'=>'required',
            'date_end'=>'required',
            'date_start'=>'required',
            'time_start'=>'required|regex:/^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]+$/',
            'time_end'=>'required|regex:/^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]+$/',
            'order'=>'required|numeric|min:1',
            'text'=>'required',
            'status'=>'required',
            'excerpt'=>'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
