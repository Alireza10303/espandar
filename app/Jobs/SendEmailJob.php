<?php

namespace App\Jobs;

use App\Mail\SendMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $exception;

    /**
     * Create a new job instance.
     * @param $exception
     */
    public function __construct($exception)
    {
        $this->exception=$exception;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        sendMailErrorController($this->exception);
    }
}
