<!DOCTYPE HTML>

<html lang="{{LaravelLocalization::getCurrentLocale()}}">
<head>


    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>پیش نمایش سپندار</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif
    <style>

        body{
            direction: rtl;
        }

        .ityped-cursor {
            font-size: 1rem;
            opacity: 1;
            -webkit-animation: blink 0.3s infinite;
            -moz-animation: blink 0.3s infinite;
            animation: blink 0.3s infinite;
            animation-direction: alternate;
        }

        @media (max-width:1200px) {
            .container > #ityped {
              display: none;
            }
            .container > .ityped-cursor {
                display: none;
            }
        }

        @keyframes blink {
            100% {
                opacity: 0;
            }
        }

        @-webkit-keyframes blink {
            100% {
                opacity: 0;
            }
        }

        @-moz-keyframes blink {
            100% {
                opacity: 0;
            }
        }

    </style>
    <!--[if lte IE 8]><script src="{{asset('template/prefix/css/ie/html5shiv.js')}}"></script><![endif]-->
    <script src="{{asset('template/prefix/js/skel.min.js')}}"></script>
    <script src="{{asset('template/prefix/js/init.js')}}"></script>
    <noscript>
        <link rel="stylesheet" href="{{asset('template/prefix/css/skel.css')}}" />
        <link rel="stylesheet" href="{{asset('template/prefix/css/style.css')}}" />
        <link rel="stylesheet" href="{{asset('template/prefix/css/style-wide.css')}}" />
        <link rel="stylesheet" href="{{asset('template/prefix/css/style-noscript.css')}}" />
    </noscript>
    <!--[if lte IE 9]><link rel="stylesheet" href="{{asset('template/prefix/css/ie/v9.css')}}" /><![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" href="{{asset('template/prefix/css/ie/v8.css')}}" /><![endif]-->
</head>
<body class="loading">
<div id="wrapper">
    <div id="bg"></div>
    <div id="overlay"></div>
    <div id="main">

        <!-- Header -->
        <header id="header">
            <div class="container">
                <span id="ityped" style="font-size: 30px"></span>
            </div>
            <br>
            <div>

                @if(!$setting->Hasmedia('logo'))
                    <img src="{{asset('img/no-img.gif')}}" alt="logo" width="200">
                @else
                    <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="logo" width="200">

                @endif
            </div>
            <div>
                <div class="social-icons social-hover top-social-list text-left">
                    <ul class="list-inline">
                        @foreach(config('cms.all-lang') as $language)
                                <li style="display: inline-block">
                                    <a href="{{env('APP_URL')."/".$language."/index"}}" style="text-decoration: none;border-bottom: none">
                                        <img src="{{asset(config('cms.flag.'.$language))}}" width="30">
                                    </a>
                                </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </header>

    </div>
</div>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script src="{{asset('template/js/jquery.3.3.1.min.js')}}"></script>
<script src="{{asset('template/js/ityped.min.js')}}"></script>
<script>
    ityped.init('#ityped', {
        strings:['شرکت سرمایه کذاری سیمان اسپندار', 'Espandar Cement Investment Company'],
        startDelay: 200,
        loop: true
    });
</script>

</body>
</html>
