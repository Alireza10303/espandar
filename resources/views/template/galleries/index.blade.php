

@extends('template.app')

@section('content')


<!--page title start-->

<section class="page-title o-hidden text-center parallaxie" data-overlay="7" data-bg-img="images/bg/02.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 ml-auto mr-auto">

            </div>
        </div>
        <nav aria-label="breadcrumb" class="page-breadcrumb">

        </nav>
    </div>
</section>

<!--page title end-->


<!--body content start-->

<div class="page-content">

    <!--portfolio start-->

    <section class="o-hidden pb-17 sm-pb-8">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <div class="portfolio-filter">
                        @foreach($all_category_galleries as $gallery)
                        <button data-filter=".{{$gallery->id}}">{{$gallery->symbol}}</button>
                        @endforeach
                        <button data-filter="" class="is-checked">{{__('cms.all')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="masonry row columns-4 no-gutters popup-gallery">
                        <div class="grid-sizer"></div>
                        @foreach($items as $item)
                        <div class="masonry-brick {{$item->category}}">
                            <div class="portfolio-item">
                                @if(!$item->Hasmedia('images'))
                                    <img src="{{asset('img/no-img.gif')}}" alt="{{$item->title}}" title="{{$item->title}}">
                                @else
                                    <img src="{{$item->getFirstMediaUrl('images')}}" alt="{{$item->title}}" title="{{$item->title}}" >
                                @endif
                                <div class="portfolio-hover">
                                    <div class="portfolio-title">
                                        <h4>{{$item->title}} </h4>
                                    </div>
                                    <div class="portfolio-icon">
                                        @if(!$item->Hasmedia('images'))
                                            <a class="popup popup-img" href="{{asset('img/no-img.gif')}}">
                                                <i class="flaticon-magnifier"></i>
                                            </a>
                                        @else
                                            <a class="popup popup-img" href="{{$item->getFirstMediaUrl('images')}}">
                                                <i class="flaticon-magnifier"></i>
                                            </a>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--portfolio end-->

</div>

<!--body content end-->



@endsection
