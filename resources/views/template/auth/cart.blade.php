@extends('template.app')


@section('content')

    @if($client->cart->items->count() > 0)
<!-- main -->
<main class="cart-page default">
    <div id="full" class="container">
        <div class="row">
            <div class="cart-page-content col-xl-9 col-lg-8 col-md-12 order-1">
                <div class="cart-page-title">
                    <h1>سبد خرید</h1>
                </div>
                <div class="row">
                    @foreach($client->cart->items as $item)
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-box">
                            <div
                                class="product-seller-details product-seller-details-item-grid">
                                                        <span class="product-main-seller"><span
                                                                class="product-seller-details-label">فروشنده:
                                                            </span>آسون کالا</span>
                                <span class="product-seller-details-badge-container"></span>
                            </div>
                            <a class="product-box-img" href="{{route('products.single',['product'=>$item->product_item->slug])}}">
                                @if(!$item->product_item->Hasmedia('images'))
                                    <img  id="img-product-zoom" src="{{asset('img/no-img.gif')}}" alt="{{$item->product_item->title}}" title="{{$item->product_item->title}}" />
                                @else
                                    <img  id="img-product-zoom" src="{{$item->product_item->getFirstMediaUrl('images')}}" alt="{{$item->title}}" title="{{$item->title}}" />
                                @endif
                            </a>
                            <div class="product-box-content">
                                <div class="product-box-content-row">
                                    <div class="product-box-title">
                                        <a href="{{route('products.single',['product'=>$item->product_item->slug])}}">
                                            {{$item->product_item->title}}
                                        </a>
                                    </div>
                                    <div class="product-box-title">
                                        @if($item->count > 1)
                                            <div style="display: flex;justify-content: space-around;align-content: center;align-items: baseline;" ><i  class="fa fa-plus inc" style="cursor: pointer" data-product="{{$item->product_item->token}}"></i><span style="margin: 10px;font-size: 12px">{{$item->count}}</span><i class="fa fa-minus dec" style="cursor: pointer" data-product="{{$item->product_item->token}}"></i></div>
                                        @else
                                            <div style="display: flex;justify-content: space-around;align-content: center;align-items: baseline;"><i  class="fa fa-plus inc" style="cursor: pointer" data-product="{{$item->product_item->token}}"></i><span style="margin: 10px;font-size: 12px">{{$item->count}}</span><i class="fa fa-trash remove" style="cursor: pointer" data-product="{{$item->product_item->token}}"></i></div>

                                        @endif
                                    </div>
                                </div>
                                <div class="product-box-row product-box-row-price">
                                    @if($item->product_item->status==1)
                                        <div class="price">
                                            <div class="price-value">
                                                <div class="price-value-wrapper">
                                                    {{number_format($item->product_item->price->amount)}} <span
                                                        class="price-currency">تومان</span>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <aside class="cart-page-aside col-xl-3 col-lg-4 col-md-6 center-section order-2">
                <div class="checkout-aside">
                    <div class="checkout-summary">
                        <div class="checkout-summary-main">
                            <ul class="checkout-summary-summary">
                                <li>
                                    <span>هزینه ارسال</span>


                                      <div class="price-send">
                                          @if(calcSend($client->cart->items)!=0)
                                          <span  class="price" style="color: red;margin: 2px">{{number_format(calcSend($client->cart->items))}}  </span>
                                              <span class="currency">تومان</span>
                                          @else
                                              <span  class="price" style="color: red;margin: 2px"></span>
                                              <span class="currency"> رایگان</span>
                                              @endif
                                      </div>



                                                    <div class="wiki-container js-dk-wiki is-right">
                                                        <div class="wiki-arrow"></div>
                                                        <p class="wiki-text">
                                                            هزینه ارسال مرسولات می‌تواند وابسته به شهر و آدرس گیرنده
                                                            متفاوت باشد. در
                                                            صورتی که هر
                                                            یک از مرسولات حداقل ارزشی برابر با ۱۰۰هزار تومان داشته باشد،
                                                            آن مرسوله
                                                            بصورت رایگان
                                                            ارسال می‌شود.<br>
                                                            "حداقل ارزش هر مرسوله برای ارسال رایگان، می تواند متغیر
                                                            باشد."
                                                        </p>
                                                    </div>
                                                </span></span>
                                </li>
                            </ul>
                            <div class="checkout-summary-devider">
                                <div></div>
                            </div>
                            <div class="checkout-summary-content">
                                <div class="checkout-summary-price-title">قیمت کالا  :</div>
                                <div class="checkout-summary-price-value">
                                    <span class="checkout-summary-price-value-amount origin-price" >{{number_format($client->cart->items->sum('price'))}}</span><i>تومان</i>
                                </div>
                                <div class="checkout-summary-price-title">مبلغ قابل پرداخت :</div>
                                <div class="checkout-summary-price-value">
                                    <span class="checkout-summary-price-value-amount total-price">{{number_format(totalPrice($client->cart->items))}}</span><i>تومان</i>
                                </div>
                                <a href="{{route('client.show.shopping')}}" class="selenium-next-step-shipping">
                                    <div class="parent-btn">
                                        <button class="dk-btn dk-btn-info">
                                            ادامه ثبت سفارش
                                            <i class="now-ui-icons shopping_basket"></i>
                                        </button>
                                    </div>
                                </a>
                                <div>
                                            <span>
                                                کالاهای موجود در سبد شما ثبت و رزرو نشده‌اند، برای ثبت سفارش مراحل بعدی
                                                را تکمیل
                                                کنید.
                                            </span>
                                    <span class="wiki wiki-holder"><span class="wiki-sign"></span>
                                                <div class="wiki-container is-right">
                                                    <div class="wiki-arrow"></div>
                                                    <p class="wiki-text">
                                                        محصولات موجود در سبد خرید شما تنها در صورت ثبت و پرداخت سفارش
                                                        برای شما رزرو
                                                        می‌شوند. در
                                                        صورت عدم ثبت سفارش، تاپ کالا هیچگونه مسئولیتی در قبال تغییر
                                                        قیمت یا موجودی
                                                        این کالاها
                                                        ندارد.
                                                    </p>
                                                </div>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="checkout-feature-aside">
                        <ul>
                            <li class="checkout-feature-aside-item checkout-feature-aside-item-guarantee">
                                هفت روز
                                ضمانت تعویض
                            </li>
                            <li class="checkout-feature-aside-item checkout-feature-aside-item-cash">
                                پرداخت در محل با
                                کارت بانکی
                            </li>
                            <li class="checkout-feature-aside-item checkout-feature-aside-item-express">
                                تحویل اکسپرس
                            </li>
                        </ul>
                    </div>
                </div>
            </aside>
        </div>
    </div>
    <div id="empty" class="container text-center" style="display: none">
        <div class="cart-empty">
            <div class="cart-empty-icon">
                <i class="now-ui-icons shopping_basket"></i>
            </div>
            <div class="cart-empty-title">سبد خرید شما خالیست!</div>

        </div>
    </div>
</main>
<!-- main -->
    @else

<!-- main -->
<main class="cart default">
    <div class="container text-center">
        <div class="cart-empty">
            <div class="cart-empty-icon">
                <i class="now-ui-icons shopping_basket"></i>
            </div>
            <div class="cart-empty-title">سبد خرید شما خالیست!</div>

        </div>
    </div>
</main>
<!-- main -->
    @endif


@endsection

@section('heads')

    <div class="loading-asonkala" style="position: absolute;background: #fdfdfd;left:0;right: 0;z-index: 9999;height: 100%;opacity: .7;display: none">
            <img src="{{$setting->getFirstMediaUrl('logo')}}"  alt="{{$setting->name}}" title="{{$setting->name}}" width="200" style="position: relative;top: 11%;right:33%; ">
    </div>
@endsection

@section('scripts')

   <script>
       function formatNumber(num) {
           return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
       }
       $('.fa.fa-plus.inc').click(function () {

           target=$(this);
           item=target.data('product');
                $.ajax({
                    type: 'GET',
                    url: '/ajax/increment/product/' + item,
                    beforeSend: function(){
                        $(".loading-asonkala").show();
                    },
                    success: function (response) {
                        console.log(response)
                        target.siblings('span').text(response.data.current.count)
                        $('.total-price').text(formatNumber(response.data.totalPrice))
                        $('.origin-price').text(formatNumber(response.data.productPrice))
                        if(response.data.sendPrice==0){
                            $('.price-send').children('span.price').text(' رایگان')
                            $('.price-send').children('span.currency').text('')
                        }else{
                            $('.price-send').children('span.price').text(formatNumber(response.data.sendPrice))
                            $('.price-send').children('span.currency').text('تومان')
                        }
                        if (response.data.current.count>=2){
                            target.siblings('i.fa-trash').removeClass().addClass('fa fa-minus dec')
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(errorThrown);
                    },
                    complete:function(data){
                        $(".loading-asonkala").hide();

                    }
                });
       });
       $('.fa.fa-minus.dec').click(function () {

           target=$(this);
           item=target.data('product');

           if(target.hasClass('dec')){
               $.ajax({
                   type: 'GET',
                   url: '/ajax/decrement/product/' + item,
                   beforeSend: function(){
                       $(".loading-asonkala").show();
                   },
                   success: function (response) {
                       console.log(response)
                       target.siblings('span').text(response.data.current.count)
                       $('.total-price').text(formatNumber(response.data.totalPrice))
                       $('.origin-price').text(formatNumber(response.data.productPrice))
                       if(response.data.sendPrice==0){
                           $('.price-send').children('span.price').text(' رایگان')
                           $('.price-send').children('span.currency').text('')
                       }else{
                           $('.price-send').children('span.price').text(formatNumber(response.data.sendPrice))
                           $('.price-send').children('span.currency').text('تومان')
                       }
                       if (response.data.current.count<2){
                           target.removeClass().addClass('fa fa-trash remove')
                       }
                   },
                   error: function (jqXHR, textStatus, errorThrown) {
                       console.log(errorThrown);
                   },
                   complete:function(data){
                       $(".loading-asonkala").hide();

                   }
               });
           }
           else if(target.hasClass('remove')){
               $.ajax({
                   type: 'GET',
                   url: '/ajax/remove/product/' + item,
                   beforeSend: function(){
                       $(".loading-asonkala").show();
                   },
                   success: function (response) {
                       console.log(response)
                       $('.total-price').text(formatNumber(response.data.totalPrice))
                       $('.origin-price').text(formatNumber(response.data.productPrice))
                       if(response.data.sendPrice==0){
                           $('.price-send').children('span.price').text(' رایگان')
                           $('.price-send').children('span.currency').text('')
                       }else{
                           $('.price-send').children('span.price').text(formatNumber(response.data.sendPrice))
                           $('.price-send').children('span.currency').text('تومان')
                       }

                       if (response.data.result>0){
                           target.parents('.product-box').hide()
                       }else{
                           $('#full').hide()
                           $('#empty').show()

                       }

                   },
                   error: function (jqXHR, textStatus, errorThrown) {
                       console.log(errorThrown);
                   },
                   complete:function(data){
                       $(".loading-asonkala").hide();

                   }
               });
           }

       });
       $('.fa.fa-trash.remove').click(function () {

           target=$(this);
           item=target.data('product');

           if(target.hasClass('dec')){
               $.ajax({
                   type: 'GET',
                   url: '/ajax/decrement/product/' + item,
                   beforeSend: function(){
                       $(".loading-asonkala").show();
                   },
                   success: function (response) {
                       console.log(response)
                       target.siblings('span').text(response.data.current.count)
                       $('.total-price').text(formatNumber(response.data.totalPrice))
                       $('.origin-price').text(formatNumber(response.data.productPrice))
                       if(response.data.sendPrice==0){
                           $('.price-send').children('span.price').text(' رایگان')
                           $('.price-send').children('span.currency').text('')
                       }else{
                           $('.price-send').children('span.price').text(formatNumber(response.data.sendPrice))
                           $('.price-send').children('span.currency').text('تومان')
                       }
                       if (response.data.current.count<2){
                           target.removeClass().addClass('fa fa-trash remove')
                       }
                   },
                   error: function (jqXHR, textStatus, errorThrown) {
                       console.log(errorThrown);
                   },
                   complete:function(data){
                       $(".loading-asonkala").hide();

                   }
               });
           }
           else if(target.hasClass('remove')){
               $.ajax({
                   type: 'GET',
                   url: '/ajax/remove/product/' + item,
                   beforeSend: function(){
                       $(".loading-asonkala").show();
                   },
                   success: function (response) {
                       console.log(response)
                       $('.total-price').text(formatNumber(response.data.totalPrice))
                       $('.origin-price').text(formatNumber(response.data.productPrice))
                       if(response.data.sendPrice==0){
                           $('.price-send').children('span.price').text(' رایگان')
                           $('.price-send').children('span.currency').text('')
                       }else{
                           $('.price-send').children('span.price').text(formatNumber(response.data.sendPrice))
                           $('.price-send').children('span.currency').text('تومان')
                       }

                       if (response.data.result>0){
                           target.parents('.product-box').hide()
                       }else{
                           $('#full').hide()
                           $('#empty').show()

                       }

                   },
                   error: function (jqXHR, textStatus, errorThrown) {
                       console.log(errorThrown);
                   },
                   complete:function(data){
                       $(".loading-asonkala").hide();

                   }
               });
           }

       });
   </script>

@endsection
