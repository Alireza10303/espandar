<!-- Start our Class Area -->
<section class="junior__classes__area section-lg-padding--top section-padding--md--bottom bg--white">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="section__title text-center">
                    <h2 class="title__line">کلاس های ما</h2>
                </div>
            </div>
        </div>
        <div class="row classes__wrap activation__one owl-carousel owl-theme clearfix mt--40">
            <!-- Start Single Classes -->
            <div class="col-lg-4 col-sm-6">
                <div class="junior__classes">
                    <div class="classes__thumb">
                        <a href="#">
                            <img src="{{asset('template/images/class/md-img/1.jpg')}}" alt="class images">
                        </a>
                    </div>
                    <div class="classes__inner">
                        <div class="classes__icon">
                            <img src="{{asset('template/images/class/star/1.png')}}" alt="starr images">
                            <span>50%</span>
                        </div>
                        <div class="class__details">
                            <h4><a href="#">کلاس نقاشی</a></h4>
                            <ul class="class__time">
                                <li>محدودیت سنی: زیره ۱۰ سال</li>
                                <li>ظرفیت کلاس : ۱۵</li>
                            </ul>
                            <div class="class__btn">
                                <a class="dcare__btn btn__gray min__height-btn" href="#">ثبت نام</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Classes -->
            <!-- Start Single Classes -->
            <div class="col-lg-4 col-sm-6">
                <div class="junior__classes">
                    <div class="classes__thumb">
                        <a href="#">
                            <img src="{{asset('template/images/class/md-img/2.jpg')}}" alt="class images">
                        </a>
                    </div>
                    <div class="classes__inner">
                        <div class="classes__icon">
                            <img src="{{asset('template/images/class/star/1.png')}}" alt="starr images">
                            <span>50%</span>
                        </div>
                        <div class="class__details">
                            <h4><a href="class-details.html">کلاس زبان</a></h4>
                            <ul class="class__time">
                                <li>محدودیت سنی: زیره ۱۰ سال</li>
                                <li>ظرفیت کلاس : ۱۵</li>
                            </ul>
                            <div class="class__btn">
                                <a class="dcare__btn btn__gray min__height-btn" href="#">ثبت نام</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Classes -->
            <!-- Start Single Classes -->
            <div class="col-lg-4 col-sm-6">
                <div class="junior__classes">
                    <div class="classes__thumb">
                        <a href="#">
                            <img src="{{asset('template/images/class/md-img/3.jpg')}}" alt="class images">
                        </a>
                    </div>
                    <div class="classes__inner">
                        <div class="classes__icon">
                            <img src="{{asset('template/images/class/star/1.png')}}" alt="starr images">
                            <span>50%</span>
                        </div>
                        <div class="class__details">
                            <h4><a href="class-details.html">کلاس رباتیک</a></h4>
                            <ul class="class__time">
                                <li>محدودیت سنی: زیره ۱۰ سال</li>
                                <li>ظرفیت کلاس : ۱۵</li>
                            </ul>
                            <div class="class__btn">
                                <a class="dcare__btn btn__gray min__height-btn" href="#">ثبت نام</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Classes -->
            <!-- Start Single Classes -->
            <div class="col-lg-4 col-sm-6">
                <div class="junior__classes">
                    <div class="classes__thumb">
                        <a href="#">
                            <img src="{{asset('template/images/class/md-img/3.jpg')}}" alt="class images">
                        </a>
                    </div>
                    <div class="classes__inner">
                        <div class="classes__icon">
                            <img src="{{asset('template/images/class/star/1.png')}}" alt="starr images">
                            <span>50%</span>
                        </div>
                        <div class="class__details">
                            <h4><a href="class-details.html">مسابقات رباتیک</a></h4>
                            <ul class="class__time">
                                <li>محدودیت سنی: زیره ۱۰ سال</li>
                                <li>ظرفیت کلاس : ۱۵</li>
                            </ul>
                            <div class="class__btn">
                                <a class="dcare__btn btn__gray min__height-btn" href="class-details.html">ثبت نام</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Classes -->
            <!-- Start Single Classes -->
            <div class="col-lg-4 col-sm-6">
                <div class="junior__classes">
                    <div class="classes__thumb">
                        <a href="#">
                            <img src="{{asset('template/images/class/md-img/1.jpg')}}" alt="class images">
                        </a>
                    </div>
                    <div class="classes__inner">
                        <div class="classes__icon">
                            <img src="{{asset('template/images/class/star/1.png')}}" alt="starr images">
                            <span>50%</span>
                        </div>
                        <div class="class__details">
                            <h4><a href="class-details.html">Swimming Classg</a></h4>
                            <ul class="class__time">
                                <li>محدودیت سنی: زیره ۱۰ سال</li>
                                <li>ظرفیت کلاس : ۱۵</li>
                            </ul>
                            <div class="class__btn">
                                <a class="dcare__btn btn__gray min__height-btn" href="class-details.html">ثبت نام</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Classes -->
        </div>
    </div>
</section>
<!-- End our Class Area -->
