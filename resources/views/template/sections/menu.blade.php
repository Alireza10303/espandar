<ul id="main-menu" class="nav navbar-nav mr-auto">
    @foreach($top_menus as $menu)
        @if(childCount($menu->id) > 0)
            <li class="nav-item"> <a class="nav-link" href="#"><span class="menu-label">{{convert_lang($menu,LaravelLocalization::getCurrentLocale(),'symbol')}} </span></a>
                <ul>
                    @foreach(child($menu->id) as $child)
                    <li><a href="{{$child->href}}">{{convert_lang($child,LaravelLocalization::getCurrentLocale(),'symbol')}}</a>
                    </li>
                    @endforeach

                </ul>
            </li>
        @else
            <li class="nav-item">
                <a class="nav-link" href="{{$menu->href}}">
                    <span class="menu-label">{{convert_lang($menu,LaravelLocalization::getCurrentLocale(),'symbol')}} </span>
                </a>

            </li>
        @endif


    @endforeach

</ul>
