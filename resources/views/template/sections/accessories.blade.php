@if(\Modules\Product\Helper\ProductHelper::hasAccessories($item))
<section class="box_style">
    <div class="container-fluid plp-container">
        <div class="plp-box">
            <strong class="section-title-inline-1">
                <h1> Accessories </h1>
            </strong>
        </div>
    </div>
    <div class="description-container">
        <div class="container">
            <div class="row">
                @foreach(\Modules\Product\Helper\ProductHelper::findAccessories($item) as $accessory)
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="col-lg-12  d-flex justify-content-center ">
                        <div class="col-lg-7 pic-container">
                            <a href="{{route('products.single',['product'=>$accessory->slug])}}">
                            <img src="{{$accessory->getFirstMediaUrl('images','thumb')}}" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-12 d-flex justify-content-center single-pic-text">
                        <h2>{{$accessory->title}}</h2>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif
