
@extends('template.app')

@section('content')


    <!--page title start-->

    <section class="page-title o-hidden text-center parallaxie" data-overlay="7" data-bg-img="{{asset('template/images/bg/02.jpg')}}">
        <div class="container">
            <div class="row">

            </div>
            <nav aria-label="breadcrumb" class="page-breadcrumb">

            </nav>
        </div>
    </section>


    <!--page title end-->


    <!--body content start-->

    <div class="page-content">

        <!--contact start-->

        <section class="contact-2">
            <div class="container">
                <div class="row pos-r">
                    <div class="col-lg-8 mr-auto">
                        <div class="contact-main">
                            <div class="messages">
                                @if(session()->has('message'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                    </div>
                                @endif
                                @include('core::layout.alert-danger')
                            </div>
                            <h6 class="title mb-4">{{__('cms.get_in_touch_with_us')}} </h6>
                            <form method="get" action="{{route('send.idea')}}">
                                <div class="form-group">
                                    <input id="form_name" type="text" name="full_name" class="form-control" placeholder="{{__('cms.full_name')}}" required="required" data-error="{{__('cms.necessary')}}" autocomplete="off">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input id="form_email" type="email" name="email" class="form-control" placeholder="{{__('cms.email')}}" required="required" data-error="{{__('cms.necessary')}}" autocomplete="off">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input id="form_phone" type="tel" name="subject" class="form-control" placeholder="{{__('cms.subject')}}" required="required" data-error="{{__('cms.necessary')}}" autocomplete="off">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <textarea id="form_message" name="message" class="form-control" placeholder="{{__('cms.message')}}" rows="4" required="required" data-error="{{__('cms.necessary')}}" ></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <input type="submit" class="btn btn-border btn-radius" value="{{__('cms.send')}}">

                            </form>
                        </div>
                    </div>
                    <div class="form-info theme-bg text-white">

    <ul class="contact-info list-unstyled mt-4">
                            <li class="mb-4"><i class="flaticon-paper-plane"></i><span>{{__('cms.address')}}:</span>
                                <p>{{convert_lang($setting,LaravelLocalization::getCurrentLocale(),'address')}}</p>
                            </li>
                            <li class="mb-4"><i class="flaticon-phone-call"></i><span>{{__('cms.phone')}}:</span><a href="tel::{{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}}">{{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}}</a>
                            </li>
                            <li><i class="flaticon-message"></i><span>{{__('cms.email')}}</span><a href="mailto::{{$setting->email}}"> {{$setting->email}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <!--contact end-->


        <!--map start-->

        <section class="o-hidden p-0" style="margin-bottom: 100px">
            <div class="container-fluid p-0">
                <div class="row contact-us-image">
                    <div class="col-sm-6">
                        <div class="map iframe-h" style="background-image:url({{asset('template/images/espandarentrance.jpg')}}) ">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="map iframe-h">
{!! $setting->google_map !!}
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <!--map end-->

    </div>

    <!--body content end-->


@endsection

