﻿@extends('template.app')

@section('content')

@include('template.sections.sliders')

    <!--body content start-->

    <div class="page-content">

@include('template.sections.properties')


@include('template.sections.services')

@include('template.sections.advantages')


@include('template.sections.agents')



  @include('template.sections.articles')


    </div>

    <!--body content end-->

@include('template.sections.brands')

@endsection

@section('heads')

    <style>

        .ityped-cursor {
            font-size: 1rem;
            opacity: 1;
            -webkit-animation: blink 0.3s infinite;
            -moz-animation: blink 0.3s infinite;
            animation: blink 0.3s infinite;
            animation-direction: alternate;
        }

        @keyframes blink {
            100% {
                opacity: 0;
            }
        }

        @-webkit-keyframes blink {
            100% {
                opacity: 0;
            }
        }

        @-moz-keyframes blink {
            100% {
                opacity: 0;
            }
        }
    </style>

@endsection

@section('scripts')

    <script src="{{asset('template/js/ityped.min.js')}}"></script>
    <script>
        message="{{convert_lang($setting,LaravelLocalization::getCurrentLocale(),'slogan')}}"
        ityped.init('#ityped', {
            strings:[message],
            startDelay: 200,
            loop: true
        });
    </script>

@endsection



